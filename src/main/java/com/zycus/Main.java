package com.zycus;

import com.zycus.constant.EnvEnum;
import com.zycus.util.ConsulUtil;
import com.zycus.util.DataSourceUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

import static com.zycus.util.QueryHelper.executeBatchQueries;

@Slf4j
public class Main
{
	public static        String      ENV;
	public static        String      CONSUL_IP_PORT;
	private static final QueryParser zspOutBoxMailQueries   = new QueryParser();
	private static final QueryParser zspOutBoxMail1Queries   = new QueryParser();
	private static final QueryParser zspTopicPersistQueries		= new QueryParser();
	private static final QueryParser zspUserSidepanelObjectQueries	= new QueryParser();
	private static final QueryParser gdsConsumerDataQueries	= new QueryParser();
	private static final QueryParser gdsProducerDataQueries	= new QueryParser();
	private static       EnvEnum     currentEnv;
	public static        String      CONFIG_PATH            = System.getProperty("CONFIG_PATH");
	private static final String      CONFIG_PROPERTIES_FILE = "config.properties";
	private static       int         batchSize     			= -1;
	public static 		 String  	 date 					= null;

	static
	{
		init();
	}

	private static void init()
	{
		log.info("Initializing the utility");
		checkEnvVariables();
		loadQueries();
		loadingTheCurrentEnvironmentData();
		ConsulUtil.initConsulUtil();
		DataSourceUtil.initDataSourceUtil();
		log.info("Initializing successful");
		log.info("Running the utility for {} setup", ENV);

	}

	private static void loadingTheCurrentEnvironmentData()
	{
		log.info("Loading {} setup data", ENV);
		try
		{
			currentEnv = EnvEnum.valueOf(ENV);
		}
		catch (Exception exception)
		{
			log.error("Something went wrong while loading {} setup data. Please check the logs and re-run the utility",
					ENV);
			log.debug("Exception in loading {} setup data", ENV, exception);
			System.exit(-1);
		}
	}

	private static void checkEnvVariables()
	{
		Properties appConfig = new Properties();
		String appConfigPropertyPath = CONFIG_PATH + File.separator + CONFIG_PROPERTIES_FILE;
		log.debug("config.properties path set as {} ", appConfigPropertyPath);
		try
		{
			appConfig.load(new FileInputStream(appConfigPropertyPath));
		}
		catch (IOException e)
		{
			log.error(
					"Something went wrong while reading config.properties. Please check the logs and re-run the utility");
			log.debug("Exception in reading config.properties ", e);
			System.exit(-1);
		}

		log.info("Checking required variables [ ZSN_SETUP_ENV , CONSUL_IP_PORT ]");
		if (!appConfig.containsKey("ZSN_SETUP_ENV") || "".equals(appConfig.getProperty("ZSN_SETUP_ENV")))
		{
			log.error("'ZSN_SETUP_ENV' is not set in config.properties. Please set and rerun the utility [ {} ]", ENV);
			System.exit(-1);
		}
		else
		{
			ENV = appConfig.getProperty("ZSN_SETUP_ENV");
			log.info("'ZSN_SETUP_ENV' is set as [ {} ]", ENV);
		}

		if (!appConfig.containsKey("CONSUL_IP_PORT") || "".equals(appConfig.getProperty("CONSUL_IP_PORT")))
		{
			log.info("'CONSUL_IP_PORT' not set. Using localhost:8500 as default");
			CONSUL_IP_PORT = "localhost:8500";
		}
		else
		{
			CONSUL_IP_PORT = appConfig.getProperty("CONSUL_IP_PORT");
			log.info("'CONSUL_IP_PORT' is set as [ {} ]", CONSUL_IP_PORT);
		}
		if (appConfig.containsKey("BATCH_SIZE") && !"".equals(appConfig.getProperty("BATCH_SIZE")))
		{
			batchSize = Integer.parseInt(appConfig.getProperty("BATCH_SIZE"));
			log.info("'BATCH_SIZE' is set as {}", batchSize);
		}
		if (appConfig.containsKey("DATE") && !"".equals(appConfig.getProperty("DATE")))
		{
			// TODO --change date to Date format
			date = appConfig.getProperty("DATE");
			log.info("'DATE' is set as {}", date);

		}
	}

	private static void loadQueries()
	{
		log.info("Loading required queries");

		try
		{
			zspOutBoxMailQueries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/zsp_outbox_mail.xml"))));
			zspOutBoxMail1Queries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/zsp_outbox_mail_1.xml"))));
			zspTopicPersistQueries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/zsp_topic_persist.xml"))));
			zspUserSidepanelObjectQueries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/zsp_user_sidepanel_object.xml"))));
			gdsConsumerDataQueries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/gds_consumer_data.xml"))));
			gdsProducerDataQueries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/gds_producer_data.xml"))));
		}
		catch (Exception exception)
		{
			log.error("Something went wrong while loading queries. Please check the logs and re-run the utility");
			log.debug("Exception in loading queries", exception);
			System.exit(-1);
		}
		log.info("Required queries loaded successfully");
	}

	public static void main(String[] args)
	{

//		TenantEnum[] tenantEnumArray = currentEnv.getTenants();
//		int noOfTenant = tenantEnumArray.length;
//		log.info("{} tenants configured for automation cleanup for {} setup", noOfTenant, ENV);
//
//		for (int i = 0; i < noOfTenant; i++)
//		{
//			TenantEnum currentTenant = tenantEnumArray[i];
//			log.info("{}/{} Starting cleanup for {}", i + 1, noOfTenant, currentTenant);
//			if (QueryHelper.validate(currentTenant))
//			{
				executeBatchQueries(zspOutBoxMailQueries, "zspOutBoxMailQueries", batchSize);
				executeBatchQueries(zspOutBoxMail1Queries, "zspOutBoxMail1Queries", batchSize);
				executeBatchQueries(zspTopicPersistQueries, "zspTopicPersistQueries", batchSize);
				executeBatchQueries(zspUserSidepanelObjectQueries, "zspUserSidepanelObjectQueries", batchSize);
				executeBatchQueries(gdsConsumerDataQueries, "gdsConsumerDataQueries", batchSize);
				executeBatchQueries(gdsProducerDataQueries, "gdsProducerDataQueries", batchSize);
//			}
//			else
//			{
//				log.info("Skipping cleanup {} tenant ", currentTenant);
//			}
//		}
		log.info("Time Based cleanup for {} setup successful", ENV);
	}
}
