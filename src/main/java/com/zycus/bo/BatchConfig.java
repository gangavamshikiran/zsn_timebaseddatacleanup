package com.zycus.bo;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
public class BatchConfig
{
	private int                 batchSize;
	private String              countQuery;
	private String              fetchBatchQuery;
	private Map<String, String> batchQuery = new LinkedHashMap<String, String>();

}
