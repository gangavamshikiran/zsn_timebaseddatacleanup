package com.zycus.util;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class PasswordProtectorUtil
{

	private PasswordProtectorUtil()
	{
		throw new IllegalStateException("Utility class");
	}

	public static final String decode(String secret)
			throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
			IllegalBlockSizeException
	{
		byte[] kbytes = "-5f0cf48fcfae2bb".getBytes();
		SecretKeySpec key = new SecretKeySpec(kbytes, "Blowfish");

		BigInteger n = new BigInteger(secret, 16);
		byte[] encoding = n.toByteArray();

		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(2, key);
		byte[] decode = cipher.doFinal(encoding);
		return new String(decode);
	}

}