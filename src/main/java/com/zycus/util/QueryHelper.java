package com.zycus.util;

import com.zycus.QueryParser;
import com.zycus.bo.BatchConfig;
import com.zycus.constant.TenantEnum;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@Slf4j
public class QueryHelper
{
	private static final String validateQuery = "SELECT tenant_id,COUNT(1) user_count FROM zsp_tms_company "
			+ "WHERE tenant_id = '#TENANT_ID#' GROUP BY tenant_id";

	public static void executeBatchQueries(QueryParser queries, String name,
			int batchSizeOverwrite)
	{
		BatchConfig batchConfig = queries.getBatchConfig();
		if (batchSizeOverwrite < 1)
		{
			batchSizeOverwrite = batchConfig.getBatchSize();
		}
		String countQuery = batchConfig.getCountQuery();
		String fetchQuery = batchConfig.getFetchBatchQuery();

		try
		{
			float totalCount = fetchTotalCount(countQuery, name + "-countQuery");
			log.debug("identified document for deletion is {} for {} ", totalCount, name );
			int numberOfChunks = totalCount == 0 ? 0 : (int) (Math.ceil(totalCount / batchSizeOverwrite) );

			Map<String, String> transformedBatchQueries = new LinkedHashMap<>();
			batchConfig.getBatchQuery().forEach((key, value) -> {
				transformedBatchQueries.put(key, transformQuery(value, String.valueOf(numberOfChunks)));
			});

			executeQueries(transformedBatchQueries, name + "-chunkingQuery");
			List<String> chunkNumberList = fetchChunkNumber(fetchQuery, "chunkNumberQuery");

			final Iterator<String> keys = queries.getKeys();
			Map<String, String> transformedQueries = new LinkedHashMap<>();
			while (keys.hasNext())
			{
				final String key = keys.next();
				final Object value = queries.getProperty(key);
				String transformQuery = String.valueOf(value);
				transformedQueries.put(key, transformQuery);
			}

			for (int i = 0; i < chunkNumberList.size(); i++)
			{
				executeQueries(transformedQueries, name, chunkNumberList, i);
			}
		}
		catch (IllegalStateException exception)
		{
			log.debug(exception.getMessage());
			log.error("Something went wrong. Skipping {} ", name);
			return;
		}

	}

	public static float fetchTotalCount(String query, String name)
	{
		boolean failed = false;
		log.info("Executing {} ", name);
		String sql = query;
		log.debug(sql);
		float count = 0L;
		try (Connection connection = DataSourceUtil.getJdbcConnection())
		{
			connection.setAutoCommit(false);
			long startTime = System.currentTimeMillis();
			try (Statement stmt = connection.createStatement(); ResultSet result = stmt.executeQuery(sql))
			{
				while (result.next())
				{
					count = result.getFloat(1);
				}
				long endTime = System.currentTimeMillis();
				log.info("Total time for {} execution is {} ms ", name, (endTime - startTime));
			}

			catch (Exception exception)
			{
				log.error("Something went wrong while loading queries. Please check the logs and re-run the utility");
				log.debug("Exception while executing for {}", name, exception);
				failed = true;
			}

		}
		catch (Exception exception)
		{
			log.error("Something went wrong while loading queries. Please check the logs and re-run the utility");
			log.info("Something went wrong. Skipping {} ", name);
			log.debug("Exception while executing {} for {}", name, exception);
			failed = true;
		}
		if (failed)
		{
			throw new IllegalStateException(String.format("Unable to execute %s query for %s", name));
		}
		return count;
	}

	public static List<String> fetchChunkNumber(String query,  String name)
	{
		boolean failed = false;
		log.info("Executing {} ", name);
		String sql = query;
		log.debug(query);
		List<String> chunkNumberList = new ArrayList<>();
		try (Connection connection = DataSourceUtil.getJdbcConnection())
		{
			connection.setAutoCommit(false);
			long startTime = System.currentTimeMillis();
			try (Statement stmt = connection.createStatement(); ResultSet result = stmt.executeQuery(sql))
			{
				while (result.next())
				{
					chunkNumberList.add(result.getString(1));
				}
				long endTime = System.currentTimeMillis();
				log.info("Total time for {} execution is {} ms", name, (endTime - startTime));
			}
			catch (Exception exception)
			{
				log.info("Something went wrong. Skipping {} ", name);
				log.debug("Exception while executing {} ", name, exception);
				failed = true;
			}
		}
		catch (Exception exception)
		{
			log.info("Something went wrong. Skipping {} ", name);
			log.debug("Exception while executing {} ", name, exception);
			failed = true;
		}
		if (failed)
		{
			throw new IllegalStateException(String.format("Unable to execute %s query ", name));
		}
		return chunkNumberList;
	}

	public static void executeQueries(Map<String, String> batchQueries,  String name)
	{
		log.info("Executing {} ", name);
		boolean failed = false;
		try (Connection connection = DataSourceUtil.getJdbcConnection())
		{
			connection.setAutoCommit(false);
			try (Statement stmt = connection.createStatement())
			{
				for (Map.Entry<String, String> entry : batchQueries.entrySet())
				{
					String query = entry.getValue();
					log.debug(query);
					long startTime = System.currentTimeMillis();
					int rowsAffected = stmt.executeUpdate(query);
					long endTime = System.currentTimeMillis();
					log.debug("queryName: {}, rowsAffected: {}, TimeTaken(ms): {}", entry.getKey(), rowsAffected, (endTime - startTime));
				}
				connection.commit();
			}
			catch (Exception exception)
			{
				log.info("Something went wrong. Skipping {} ", name);
				log.debug("Exception while executing {} ", name, exception);
				connection.rollback();
				log.info("Changes Rolled back");
				failed = true;
			}
		}
		catch (Exception exception)
		{
			log.info("Something went wrong. Skipping {} ", name);
			log.debug("Exception while executing {} ", name,  exception);
			failed = true;
		}
		if (failed)
		{
			throw new IllegalStateException(String.format("Unable to execute %s query ", name));
		}
	}

	public static void executeQueries(Map<String, String> transformedQueries, String name,
			List<String> chunkNumberList, int currentChunkNumber)
	{
		log.info(" [{}/{}] Executing {} ", chunkNumberList.get(currentChunkNumber),
				chunkNumberList.get(chunkNumberList.size() - 1), name);
		long startTimeMain = System.currentTimeMillis();
		try (Connection connection = DataSourceUtil.getJdbcConnection())
		{
			connection.setAutoCommit(false);
			try (Statement stmt = connection.createStatement())
			{
				final Iterator<String> keys = transformedQueries.keySet().iterator();
				while (keys.hasNext())
				{
					final String key = keys.next();
					final String value = transformedQueries.get(key);
					String sql = transformChunkQuery(value, chunkNumberList.get(currentChunkNumber));
					log.debug("Now executing {} {}", key, sql);
					long startTime = System.currentTimeMillis();
					int rowsAffected = stmt.executeUpdate(sql);
					long endTime = System.currentTimeMillis();
					log.debug("queryName: {}, rowsAffected: {}, TimeTaken(ms): {}", key, rowsAffected, (endTime - startTime));
				}
				connection.commit();
			}
			catch (Exception exception)
			{
				log.info("Something went wrong. Skipping {} ", name);
				log.debug("Exception while executing {} ", name, exception);
				connection.rollback();
				log.info("Changes Rolled Back");
			}
		}
		catch (Exception exception)
		{
			log.info("Something went wrong. Skipping {}", name);
			log.debug("Exception while executing {} ", name,  exception);
		}

		long endTimeMain = System.currentTimeMillis();
		log.info(" [{}/{}] Total time for {} execution is {} ms ", chunkNumberList.get(currentChunkNumber),
				chunkNumberList.get(chunkNumberList.size() - 1), name,  endTimeMain - startTimeMain);

	}

	public static boolean validate(TenantEnum tenant)
	{
		log.info("Validating data for {} tenant", tenant);
		String sql = transformQuery(validateQuery, tenant);
		try (Connection connection = DataSourceUtil.getJdbcConnection();
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(sql))
		{
			connection.setAutoCommit(false);

			while (rs.next())
			{
				String tenantId = rs.getString(1);
				int noOfUsers = rs.getInt(2);
				log.debug("TenantId {} and NoOfUsers {}", tenantId, noOfUsers);
				if (noOfUsers > 0)
				{
					log.info("Validation successful for {} tenant", tenant);
					return true;
				}
			}

		}
		catch (Exception ex)
		{
			log.debug("Exception while validating data for {} tenant", tenant, ex);
		}
// To Do - needs to change validation Query
		return false;
//		return true;
	}

	public static String transformQuery(String query, String chunkNumber)
	{
		String transformedQuery = query;
		return transformChunkQuery(transformedQuery, chunkNumber);
	}

	public static String transformQuery(String query, TenantEnum tenantEnum)
	{
		String transformedQuery = query.replaceAll("#TENANT_ID#", tenantEnum.getId());
		//	removed created_by list
//		transformedQuery = transformedQuery.replaceAll("#CREATED_BY#", String.join(", ", tenantEnum.getUserIdList()));
		return transformedQuery;
	}

	public static String transformChunkQuery(String query, String chunkNumber)
	{
		return query.replaceAll("#CHUNK_NUMBER#", chunkNumber);
	}
}
