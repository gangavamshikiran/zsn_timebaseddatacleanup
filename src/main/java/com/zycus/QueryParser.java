package com.zycus;

import com.zycus.bo.BatchConfig;
import com.zycus.constant.QueryTypeEnum;
import org.apache.commons.configuration2.XMLPropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.Reader;

public class QueryParser extends XMLPropertiesConfiguration
{
	private BatchConfig batchConfig = new BatchConfig();

	public BatchConfig getBatchConfig()
	{
		return batchConfig;
	}

	@Override
	public void read(Reader in) throws ConfigurationException
	{
		final SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(false);
		factory.setValidating(true);

		try
		{
			final SAXParser parser = factory.newSAXParser();

			final XMLReader xmlReader = parser.getXMLReader();
			xmlReader.setEntityResolver((publicId, systemId) -> new InputSource(
					getClass().getClassLoader().getResourceAsStream("queries.dtd")));
			xmlReader.setContentHandler(new XMLPropertiesHandler());
			xmlReader.parse(new InputSource(in));
		}
		catch (final Exception e)
		{
			throw new ConfigurationException("Unable to parse the configuration file", e);
		}
	}

	private class XMLPropertiesHandler extends DefaultHandler
	{
		private String        queryName;
		private String        batchQueryName;
		private QueryTypeEnum queryType;
		private int           batchSize;
		private String        countQuery;

		private StringBuilder value = new StringBuilder();

		private boolean inQueryElement;

		private boolean inCountQueryElement;

		private boolean inFetchBatchQueryElement;

		private boolean inBatchingQueryElement;

		@Override
		public void startElement(final String uri, final String localName, final String qName, final Attributes attrs)
		{
			if ("batchConfig".equals(qName))
			{
				batchSize = Integer.parseInt(attrs.getValue("batchSize"));
				batchConfig.setBatchSize(batchSize);
			}

			if ("query".equals(qName))
			{
				queryName = attrs.getValue("name");
				queryType = QueryTypeEnum.valueOf(attrs.getValue("type"));
				inQueryElement = true;
			}

			if ("countQuery".equals(qName))
			{
				inCountQueryElement = true;
			}

			if ("fetchBatchQuery".equals(qName))
			{
				inFetchBatchQueryElement = true;
			}

			if ("batchingQuery".equals(qName))
			{
				inBatchingQueryElement = true;
				batchQueryName = attrs.getValue("name");
			}
		}

		@Override
		public void endElement(final String uri, final String localName, final String qName)
		{

			if (inQueryElement)
			{
				addProperty(queryName, value.toString());
				inQueryElement = false;
			}

			if (inCountQueryElement)
			{
				countQuery = value.toString();
				batchConfig.setCountQuery(countQuery);
				inCountQueryElement = false;
			}

			if (inFetchBatchQueryElement)
			{

				String fetchBatchQuery = value.toString();
				batchConfig.setFetchBatchQuery(fetchBatchQuery);
				inFetchBatchQueryElement = false;
			}
			if (inBatchingQueryElement)
			{

				String batchQuery = value.toString();
				batchConfig.getBatchQuery().put(batchQueryName, batchQuery);
				inBatchingQueryElement = false;
			}

			value = new StringBuilder();
		}

		@Override
		public void characters(final char[] chars, final int start, final int length)
		{
			value.append(chars, start, length);
		}
	}
}
